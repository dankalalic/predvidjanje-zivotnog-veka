# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 13:05:19 2021

@author: Dragan
"""


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import statistics

pd.set_option("display.max_rows", None, "display.max_columns", None)

df = pd.read_csv('Life Expectancy Data.csv')

df.columns = df.columns.str.strip() 

#%%

import time
def time_convert(sec):
  mins = sec // 60
  sec = sec % 60
  hours = mins // 60
  mins = mins % 60
  print("Time Lapsed = {0}:{1}:{2}".format(int(hours),int(mins),sec))
  
#%% Kategoricka i numericka obelezja

print("Broj obelezja je: ", df.shape[1], ", a broj uzoraka je: ", df.shape[0])

categorical_features = np.concatenate((df.dtypes[df.dtypes == "int64"].index, df.dtypes[df.dtypes == "object"].index))
numerical_features = (df.dtypes[df.dtypes != "int64"].index & df.dtypes[df.dtypes != "object"].index)

print("\nKategoricka obelezja su: ")
for cat in categorical_features:
    print(cat, end = ";  ")
    
print("\n\nNumericka obelezja su: ")
for num in numerical_features:
    print(num, end = ";  ")

#%%
#Svaka cifra proverava se u zavisnosti od prethodne (po drzavi)
#Prolazi se dva puta (ukoliko se negde nije stigla prvi put promeniti vrednost)
def addfigures(col):
    start_time = time.time()
    last_country = ''

    for index, row in df[col].iteritems():
        country = df.loc[index, 'Country']
        if country != last_country:
            max_digits_number = 0
        if math.isnan(row) != True:
            check=row
            while True: 
                check_dec, check_int = math.modf(check)[0], math.modf(check)[1]
                difference = abs(check-max_digits_number)
                difference2 = abs(check_int*10+check_dec-max_digits_number)
                
                if (difference2<difference):
                    check=check_int*10 + check_dec  
                else:
                   df.at[index,col] = check
                   break
            max_digits_number = check
        last_country=country
    end_time = time.time()
    time_lapsed = end_time - start_time
    time_convert(time_lapsed)

for i in ['Adult Mortality', 'Population', 'Diphtheria', 'BMI', 'Hepatitis B',
          'Polio', 'Total expenditure', 'GDP', 'thinness 5-9 years', 'thinness  1-19 years', 
          'percentage expenditure']:
    addfigures(i)
    df=df.iloc[::-1]
    addfigures(i)
    df=df.iloc[::-1]

#%%
print(df.describe())
#%% U ovim kolonama su nan vrednosti predstavljene nulama

for i in ['percentage expenditure', 'Income composition of resources', 'Schooling']:
    df[i]=df[i].replace(0, np.nan)
#%% Ukoliko u nekim kolonama postoji null vrednost, ona se popuni prethodnom ili narednom vrednoscu te iste drzave
    
def fillNullValuesWithBeforeAfterValue(col_name):
    start_time = time.time()
    df[col_name] = df.groupby('Country')[col_name].transform(lambda v: v.ffill())
    df[col_name] = df.groupby('Country')[col_name].transform(lambda v: v.bfill())
    end_time = time.time()
    time_lapsed = end_time - start_time
    time_convert(time_lapsed)
    
for i in ['Life expectancy', 'Alcohol', 'Adult Mortality', 'Population', 'Diphtheria', 'BMI', 'Hepatitis B', 'Polio', 'Total expenditure',
      'GDP', 'thinness 5-9 years', 'thinness  1-19 years', 'percentage expenditure', 'Schooling', 'Income composition of resources']:   
    fillNullValuesWithBeforeAfterValue(i)
#%% Obrisu se svi uzorci kojima fali vise od dve vrednosti
    
def checkRowsWithMissingValues(df):
    items=[]
    index_row=0
    
    for index, row in df.iterrows():
        n=0
        index_col=0
        
        for columnIndex,value in row.items():
        
            if pd.isnull(df.iloc[index_row,index_col]):
                n=n+1
                if n>2:
                    items.append(index)
                    break                
            index_col=index_col+1
        index_row=index_row+1
    return(items)

print(checkRowsWithMissingValues(df))
lista=checkRowsWithMissingValues(df)
print(len(lista))

#%% Ostavljaju se samo uzorci gde fali podatak za populaciju (i oni kojima ne fali nijedan podatak)
df = df.drop(lista)
df = df[df['Hepatitis B'].notna()]

print("Broj obelezja je: ", df.shape[1], ", a broj uzoraka je: ", df.shape[0])

#%% Sve se eksportuje u novu tabelu

df.to_csv('new_table.csv')

#%%Popunjavanje onih uzoraka gde fale podaci samo za populaciju - rucno

df=pd.read_csv('new_table_filled_values.csv')

print(df.isnull().sum())

#%%

