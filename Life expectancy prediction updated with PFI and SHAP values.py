import pandas as pd
import numpy as np
import math
from scipy.stats import iqr
import time
from sklearn.model_selection import GroupShuffleSplit
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.preprocessing import StandardScaler, PolynomialFeatures
from sklearn.decomposition import PCA
from sklearn.model_selection import GridSearchCV
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression, Ridge, Lasso
import shap
import matplotlib.pyplot as plt

pd.set_option("display.max_rows", None, "display.max_columns", None)

df = pd.read_csv('Life Expectancy Data.csv')

df.columns = df.columns.str.strip() 

#%% Merenje vremena
def time_convert(sec):
  mins = sec // 60
  sec = sec % 60
  hours = mins // 60
  mins = mins % 60
  print("Time Lapsed = {0}:{1}:{2}".format(int(hours),int(mins),sec))
  
#%% Kategoricka i numericka obelezja

print("Broj obelezja je: ", df.shape[1], ", a broj uzoraka je: ", df.shape[0])

categorical_features = np.concatenate((df.dtypes[df.dtypes == "int64"].index, df.dtypes[df.dtypes == "object"].index))
numerical_features = (df.dtypes[df.dtypes != "int64"].index & df.dtypes[df.dtypes != "object"].index)

print("\nKategoricka obelezja su: ")
for cat in categorical_features:
    print(cat, end = ";  ")
    
print("\n\nNumericka obelezja su: ")
for num in numerical_features:
    print(num, end = ";  ")

#%%
#Svaka cifra proverava se u zavisnosti od prethodne (po drzavi)
#Prolazi se dva puta (ukoliko se negde nije stigla prvi put promeniti vrednost)
def addfigures(col):
    start_time = time.time()
    last_country = ''

    for index, row in df[col].iteritems():
        country = df.loc[index, 'Country']
        if country != last_country:
            max_digits_number = 0
        if math.isnan(row) != True:
            check=row
            while True: 
                check_dec, check_int = math.modf(check)[0], math.modf(check)[1]
                difference = abs(check-max_digits_number)
                difference2 = abs(check_int*10+check_dec-max_digits_number)
                
                if (difference2<difference):
                    check=check_int*10 + check_dec  
                else:
                   df.at[index,col] = check
                   break
            max_digits_number = check
        last_country=country
    end_time = time.time()
    time_lapsed = end_time - start_time
    time_convert(time_lapsed)

for i in ['Adult Mortality', 'Population', 'Diphtheria', 'BMI', 'Hepatitis B',
          'Polio', 'Total expenditure', 'GDP', 'thinness 5-9 years', 'thinness  1-19 years', 
          'percentage expenditure']:
    addfigures(i)
    df=df.iloc[::-1]
    addfigures(i)
    df=df.iloc[::-1]

#%% Provera gde postoje nule koje ne bi trebalo da postoje
print(df.describe())

#%% U ovim kolonama su nan vrednosti predstavljene nulama
for i in ['percentage expenditure', 'Income composition of resources', 'Schooling']:
    df[i]=df[i].replace(0, np.nan)
    
#%%
print(df.isna().mean())
#%% Ukoliko u nekim kolonama postoji null vrednost, ona se popuni prethodnom ili narednom vrednoscu te iste drzave
def fillNullValuesWithBeforeAfterValue(col_name):
    start_time = time.time()
    df[col_name] = df.groupby('Country')[col_name].transform(lambda v: v.ffill())
    df[col_name] = df.groupby('Country')[col_name].transform(lambda v: v.bfill())
    end_time = time.time()
    time_lapsed = end_time - start_time
    time_convert(time_lapsed)
    
for i in ['Life expectancy', 'Alcohol', 'Adult Mortality', 'Population', 'Diphtheria', 'BMI', 'Hepatitis B', 'Polio', 'Total expenditure',
      'GDP', 'thinness 5-9 years', 'thinness  1-19 years', 'percentage expenditure', 'Schooling', 'Income composition of resources']:   
    fillNullValuesWithBeforeAfterValue(i)
#%% Obrisu se svi uzorci kojima fali vise od dve vrednosti
def checkRowsWithMissingValues(df):
    items=[]
    index_row=0
    
    for index, row in df.iterrows():
        n=0
        index_col=0
        for columnIndex,value in row.items():
            if pd.isnull(df.iloc[index_row,index_col]):
                n=n+1
                if n>2:
                    items.append(index)
                    break                
            index_col=index_col+1
        index_row=index_row+1
    return(items)

print(checkRowsWithMissingValues(df))
lista=checkRowsWithMissingValues(df)
print(len(lista))

#%% Ostavljaju se samo uzorci gde fali podatak za populaciju (i oni kojima ne fali nijedan podatak)
df = df.drop(lista)
df = df[df['Hepatitis B'].notna()]

print("Broj obelezja je: ", df.shape[1], ", a broj uzoraka je: ", df.shape[0])

#%% Sve se eksportuje u novu tabelu
df.to_csv('new_table.csv')

#%%Popunjavanje onih uzoraka gde fale podaci samo za populaciju, developing u developed za kanadu i francusku + thinness 1-19 promenjen za Filipine

df=pd.read_csv('new_table_filled_values.csv')

print(df.isnull().sum())

# Podaci za measles su zapravo ukupan broj slucajeva
df['Measles']=df['Measles']/df['Population']

# Status promena u numericke vrednosti
df["Status"].replace({"Developing":0, "Developed":1}, inplace=True)

# Brisanje kolone koja oznacava indekse
df.drop(df.columns[0], axis=1,inplace=True)

#%% Statisticka analiza, iqr i dinamicki opsezi

print(df.describe())

#IQR opseg
iqr_x = iqr(df.loc[:, df.columns != 'Country'], axis = 0)
np.set_printoptions(formatter={'float_kind':'{:f}'.format})
print("Interkvartilni opseg redom za obelezja", iqr_x)


#Dinamicki opseg
din_x = []
for (columnName, columnData) in df.loc[:, df.columns != 'Country'].iteritems(): 
    din_x.append(df[columnName].max() - df.loc[:, df.columns != 'Country'][columnName].min())

print("Dinamicki opseg redom za obelezja", din_x)

#%% Kreiranje trening i test skupa
x=df.drop(['Life expectancy'], axis=1)
y=df["Life expectancy"]

#x=x.apply(pd.to_numeric)
#y=y.apply(pd.to_numeric)

#Isprobati razlicite test_size 
#X_train, X_test, Y_train, Y_test = train_test_split(x, y, test_size=0.1, random_state=35)
gss = GroupShuffleSplit(n_splits=1, test_size=0.1, random_state=42)
X_train, X_test, Y_train, Y_test = pd.DataFrame(columns=x.columns), pd.DataFrame(columns=x.columns), pd.Series(), pd.Series()

for train_idx, test_idx in gss.split(x, y, x['Country']):
    for i in train_idx:
        X_train = X_train.append(x.loc[x.index==i])
        Y_train = Y_train.append(y.loc[y.index==i])
    for i in test_idx:
        X_test = X_test.append(x.loc[x.index==i])
        Y_test = Y_test.append(y.loc[y.index==i])

"""X_train.reset_index(drop=True, inplace=True)
X_test.reset_index(drop=True, inplace=True)
Y_train.reset_index(drop=True, inplace=True)
Y_test.reset_index(drop=True, inplace=True)"""
#%% Evaluacija modela

def model_evaluation(y_val, y_predicted, N, d):
    mse = mean_squared_error(y_val, y_predicted)
    mae = mean_absolute_error(y_val, y_predicted)
    rmse = np.sqrt(mse)
    r2 = r2_score(y_val, y_predicted)
    r2_adj = 1-(1-r2)*(N-1)/(N-d-1)
    rss = np.sum(np.square(y_predicted - y_val))
    
    #res=pd.concat([pd.DataFrame(y.values), pd.DataFrame(y_predicted)], axis=1)
    #res.columns = ['y', 'y_pred']
    #print(res.head(15))
    
    return [mse, mae, rmse, r2, r2_adj, rss]

def model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss):
    print('Mean squared error: ', mse)
    print('Mean absolute error: ', mae)
    print('Root mean squared error: ', rmse)
    print('R2 score: ', r2)
    print('R2 adjusted score: ', r2_adj)
    print("RSS: ", rss)
#%% Scorer za GridSearchCV
def adj_r2(estimator, X, y_true):
    n, p = X.shape
    pred = estimator.predict(X)
    return 1 - ((1 - r2_score(y_true, pred)) * (n - 1))/(n-p-1)

scoring = {'mae': 'neg_mean_absolute_error', 'r2_adj' : adj_r2}

#%%
X_train_copy = X_train
X_train = X_train.drop(['Country'], axis=1)
X_test = X_test.drop(['Country'], axis=1)

#%% Normalizacija obelezja

s = StandardScaler()
x_train_std = s.fit_transform(X_train)
x_test_std = s.fit_transform(X_test)
x_train_std = pd.DataFrame(x_train_std)
x_test_std = pd.DataFrame(x_test_std)
x_train_std.columns = X_train.columns
x_test_std.columns = X_test.columns

#%% PCA

pca = PCA(n_components=0.9, random_state=42)
pca.fit(x_train_std)
X_train_r = pca.transform(x_train_std)
X_test_r = pca.transform(x_test_std)

X_train_r=pd.DataFrame(X_train_r)
X_test_r=pd.DataFrame(X_test_r)
  
X_train_r = X_train_r.rename(columns = {0: 'Feature 0', 1: 'Feature 1', 2: 'Feature 2', 3: 'Feature 3',
                                        4: 'Feature 4', 5: 'Feature 5', 6: 'Feature 6', 
                                        7: 'Feature 7', 8: 'Feature 8'}, inplace = False)
X_test_r = X_test_r.rename(columns = {0: 'Feature 0', 1: 'Feature 1', 2: 'Feature 2', 3: 'Feature 3',
                                        4: 'Feature 4', 5: 'Feature 5', 6: 'Feature 6', 
                                        7: 'Feature 7', 8: 'Feature 8'}, inplace = False)

print('Redukovani prostor ima dimenziju: ', pca.n_components_)

#%% Stabla odluke
gss = GroupShuffleSplit(n_splits=10, test_size=0.1, random_state=35)
gss = gss.split(X_train, Y_train, X_train_copy['Country'])

parameters = {'criterion':('mse', 'friedman_mse', 'mae', 'poisson'),'max_features':('auto','sqrt','log2'),
              'splitter' : ('best', 'random'), 'max_depth':(15, 20, 25, 30, None)}
rf = DecisionTreeRegressor(random_state=35)
clf=GridSearchCV(rf, parameters, scoring=scoring, cv=gss, refit='mae')
clf.fit(x_train_std, Y_train)
print(clf.best_params_)
print(clf.best_score_)
#%% Stabla odluke PCA
gss = GroupShuffleSplit(n_splits=10, test_size=0.1, random_state=35)
gss = gss.split(X_train, Y_train, X_train_copy['Country'])
parameters = {'criterion':('mse', 'friedman_mse', 'mae', 'poisson'),'max_features':('auto','sqrt','log2'),
              'splitter' : ('best', 'random'), 'max_depth':(15, 20, 25, 30, None)}
rf = DecisionTreeRegressor(random_state=35)
clf=GridSearchCV(rf, parameters, scoring=scoring, cv=gss, refit='mae')
clf.fit(X_train_r, Y_train)
print(clf.best_params_)
print(clf.best_score_)

#%% Random forest
gss = GroupShuffleSplit(n_splits=10, test_size=0.1, random_state=35)
gss = gss.split(X_train, Y_train, X_train_copy['Country'])
parameters = {'criterion':('mse', 'mae'),'max_features':('auto','sqrt','log2')}
rf = RandomForestRegressor(n_estimators=60, random_state=35)
clf=GridSearchCV(rf, parameters, scoring=scoring, cv=gss, refit='mae')
clf.fit(x_train_std, Y_train)
print(clf.best_params_)
print(clf.best_score_)

#%% Random forest PCA
gss = GroupShuffleSplit(n_splits=10, test_size=0.1, random_state=35)
gss = gss.split(X_train, Y_train, X_train_copy['Country'])
parameters = {'criterion':('mse', 'mae'),'max_features':('auto','sqrt','log2')}
rf = RandomForestRegressor(n_estimators=100, random_state=35)
clf=GridSearchCV(rf, parameters, scoring=scoring, cv=gss, refit='mae')
clf.fit(X_train_r, Y_train)
print(clf.best_params_)
print(clf.best_score_)

#%% KNN
gss = GroupShuffleSplit(n_splits=10, test_size=0.1, random_state=35)
gss = gss.split(X_train, Y_train, X_train_copy['Country'])
parameters = {'metric':('euclidean', 'manhattan', 'chebyshev','hamming', 'canberra', 'braycurtis'),
      'n_neighbors':(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), 
      'weights':('uniform', 'distance')}

knn = KNeighborsRegressor()
clf=GridSearchCV(knn, parameters, scoring=scoring, cv=gss, refit='mae')
clf.fit(x_train_std, Y_train)
print(clf.best_params_)
print(clf.best_score_)

#%% KNN PCA
gss = GroupShuffleSplit(n_splits=10, test_size=0.1, random_state=35)
gss = gss.split(X_train, Y_train, X_train_copy['Country'])
parameters = {'metric':('euclidean', 'manhattan', 'chebyshev','hamming', 'canberra', 'braycurtis'),
      'n_neighbors':(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), 
      'weights':('uniform', 'distance')}

knn = KNeighborsRegressor()
clf=GridSearchCV(knn, parameters, scoring=scoring, cv=gss, refit='mae0')
clf.fit(X_train_r, Y_train)
print(clf.best_params_)
print(clf.best_score_)

#%% Funkcije za linearnu regresiju
#%% Obicna linearna regresija

def linearRegression(x_train, y_train):
    
    print("**********************Linearna regresija**********************")

    gss = GroupShuffleSplit(n_splits=10, test_size=0.1, random_state=35)
    gss = gss.split(X_train, Y_train, X_train_copy['Country'])

    [mse, mae, rmse, r2, r2_adj, rss] = 0, 0, 0, 0, 0, 0
    for train_index, test_index in gss:
        linear_regression_model = LinearRegression(fit_intercept=True)
        linear_regression_model.fit(x_train.iloc[train_index,:], y_train.iloc[train_index])
        Y_pred = linear_regression_model.predict(x_train.iloc[test_index,:])
        
        [mse1, mae1, rmse1, r21, r2_adj1, rss1] = model_evaluation(y_train.iloc[test_index], Y_pred, x_train.iloc[train_index,:].shape[0], x_train.iloc[train_index,:].shape[1])
        #Dodajemo ove greske na greske iz prethodnih iteracija
        [mse, mae, rmse, r2, r2_adj, rss] = tuple(ele1 + ele2 for ele1, ele2 in zip([mse, mae, rmse, r2, r2_adj, rss], [mse1, mae1, rmse1, r21, r2_adj1, rss1]))
    #Nalazimo srednju vrednost greske
    #[mse, mae, rmse, r2, r2_adj, rss] = tuple([x/10 for x in [mse, mae, rmse, r2, r2_adj, rss]])
    mse, mae, rmse, r2, r2_adj, rss = mse/10, mae/10, rmse/10, r2/10, r2_adj/10, rss/10
    
    model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)
    return [mse, mae, rmse, r2, r2_adj, rss]

#%%Ridge regresija

def ridgeRegression(x_train, y_train, alpha):
    
    print("**********************Ridge regresija**********************")
    
    gss = GroupShuffleSplit(n_splits=10, test_size=0.1, random_state=35)
    gss = gss.split(X_train, Y_train, X_train_copy['Country'])
    
    [mse, mae, rmse, r2, r2_adj, rss] = 0, 0, 0, 0, 0, 0
    for train_index, test_index in gss:   
        
        ridge_model = Ridge(alpha=alpha)
        ridge_model.fit(x_train.iloc[train_index,:], y_train.iloc[train_index])
        Y_pred = ridge_model.predict(x_train.iloc[test_index,:])
        
        [mse1, mae1, rmse1, r21, r2_adj1, rss1] = model_evaluation(y_train.iloc[test_index], Y_pred, x_train.shape[0], x_train.shape[1])
        #Dodajemo ove greske na greske iz prethodnih iteracija
        [mse, mae, rmse, r2, r2_adj, rss] = tuple(ele1 + ele2 for ele1, ele2 in zip([mse, mae, rmse, r2, r2_adj, rss], [mse1, mae1, rmse1, r21, r2_adj1, rss1]))
    #Nalazimo srednju vrednost greske
    [mse, mae, rmse, r2, r2_adj, rss] = tuple([x/10 for x in [mse, mae, rmse, r2, r2_adj, rss]])
    model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)
    return [mse, mae, rmse, r2, r2_adj, rss]   

#%% Lasso regresija
    
def lassoRegression(x_train, y_train, alpha, max_iter=1000):
    
    print("**********************Lasso regresija**********************")
    
    gss = GroupShuffleSplit(n_splits=10, test_size=0.1, random_state=35)
    gss = gss.split(X_train, Y_train, X_train_copy['Country'])
    
    [mse, mae, rmse, r2, r2_adj, rss] = 0, 0, 0, 0, 0, 0
    for train_index, test_index in gss:   
        
        lasso_model = Lasso(alpha=alpha, max_iter=max_iter)
        lasso_model.fit(x_train.iloc[train_index,:], y_train.iloc[train_index])
        Y_pred = lasso_model.predict(x_train.iloc[test_index,:])
        
        [mse1, mae1, rmse1, r21, r2_adj1, rss1] = model_evaluation(y_train.iloc[test_index], Y_pred, x_train.shape[0], x_train.shape[1])
        #Dodajemo ove greske na greske iz prethodnih iteracija
        [mse, mae, rmse, r2, r2_adj, rss] = tuple(ele1 + ele2 for ele1, ele2 in zip([mse, mae, rmse, r2, r2_adj, rss], [mse1, mae1, rmse1, r21, r2_adj1, rss1]))
    #Nalazimo srednju vrednost greske
    [mse, mae, rmse, r2, r2_adj, rss] = tuple([x/10 for x in [mse, mae, rmse, r2, r2_adj, rss]])
    model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)
    return [mse, mae, rmse, r2, r2_adj, rss]   

#%% Proizvoljna polinomna hipoteza
def polynomialHypothesis(x_train, x_test, interactionsOnly, degree):    

    poly = PolynomialFeatures(degree=degree, interaction_only=interactionsOnly, include_bias=False)
    
    x_inter_train = poly.fit_transform(x_train)
    x_inter_test = poly.transform(x_test)
    
    feature_names_train = poly.get_feature_names(x_train.columns)
    feature_names_test = poly.get_feature_names(x_test.columns)
    
    x_inter_train_df = pd.DataFrame(data=x_inter_train, columns = feature_names_train)
    x_inter_test_df = pd.DataFrame(data=x_inter_test, columns = feature_names_test)
    
    x_inter_train_df.columns = x_inter_train_df.columns.astype(str)
    x_inter_test_df.columns = x_inter_test_df.columns.astype(str)
        
    return x_inter_train_df, x_inter_test_df

numFeatures = len(x_train_std.columns)

x_train_intOnly2, x_test_intOnly2 = polynomialHypothesis(x_train_std, x_test_std, interactionsOnly=True, degree=2)
x_train_int2, x_test_int2 = polynomialHypothesis(x_train_std, x_test_std, interactionsOnly=False, degree=2)

x_train_intOnly2PCA, x_test_intOnly2PCA = polynomialHypothesis(X_train_r, X_test_r, interactionsOnly=True, degree=2)
x_train_int2PCA, x_test_int2PCA = polynomialHypothesis(X_train_r, X_test_r, interactionsOnly=False, degree=2)

x_train_intOnly3, x_test_intOnly3 = polynomialHypothesis(x_train_std, x_test_std, interactionsOnly=True, degree=3)
x_train_intOnly3PCA, x_test_intOnly3PCA = polynomialHypothesis(X_train_r, X_test_r, interactionsOnly=True, degree=3)

#%% Linearna, obicna hipoteza
linearRegression(x_train_std, Y_train)
print("\nPCA\n")
linearRegression(X_train_r, Y_train)

#%% Linearna, drugi stepen samo interakcije 
linearRegression(x_train_intOnly2, Y_train)
print("\nPCA\n")
linearRegression(x_train_intOnly2PCA, Y_train)

#%% Ridge, obicna hipoteza
ridgeRegression(x_train_std, Y_train, 5)
print("\nPCA\n")
ridgeRegression(X_train_r, Y_train, 450)

#%% Ridge, drugi stepen samo interakcije
ridgeRegression(x_train_intOnly2,Y_train, 200)
print("\nPCA\n")
ridgeRegression(x_train_intOnly2PCA,Y_train, 1000)

#%% Lasso, obicna hipoteza 
lassoRegression(x_train_std, Y_train, 0.1)
print("\nPCA\n")
lassoRegression(X_train_r, Y_train, 0.1)

#%% Lasso, drugi stepen, samo interakcije
lassoRegression(x_train_intOnly2, Y_train, 0.1) #najbolje
print("\nPCA\n")
lassoRegression(x_train_intOnly2PCA, Y_train, 0.2)

#%% Lasso, drugi stepen
lassoRegression(x_train_int2, Y_train, 0.1)
print("\nPCA\n")
lassoRegression(x_train_int2PCA, Y_train, 0.1) #najbolje

#%% Permutation feature importance

from sklearn.inspection import permutation_importance

def perm_importance(model, x, y, imgtext, lr, plotline=False):
    r = permutation_importance(model, x, y, n_repeats=30, random_state=0, scoring='neg_mean_absolute_error')
    
    for i in r.importances_mean.argsort()[::-1]:
        print(f"{x.columns[i]:<}" + " " +
        f"{r.importances_mean[i]:.3f}"
        f" +/- {r.importances_std[i]:.3f}")
        

    xdata=x.columns
    rdata = r.importances_mean
    data = {'Feature' : xdata, 'Importance': rdata}
    df_new = pd.DataFrame(data)
    #print(df_new)
    df_sorted = df_new.sort_values('Importance')
    if lr == True:
        df_sorted = pd.concat([df_sorted[:10], df_sorted[-10:]])
    print(df_sorted)
    plt.figure()
    pltt = df_sorted.plot(kind='barh', x='Feature', y='Importance')
    for i, v in enumerate(df_sorted['Importance']):
        pltt.text(v + .1, i - .15, str(round(v, 4)), color='blue')
    plt.title('Permutation feature importance')
    plt.ylabel('Feature name')
    plt.xlabel('Feature importance')
    if plotline == True:
        plt.axhline(y=9.65, color='b', linestyle='dashed', linewidth=0.5)
    
    plt.savefig(imgtext, bbox_inches="tight")
 
#%% SHAP values
    
def shapValuesTree(regressor, xtrain, xtest, imgSummaryBar, imgSummary, imgWaterfall, imgForce):
    
    explainer = shap.TreeExplainer(regressor, xtrain)
    shap_values = explainer.shap_values(xtest)
    
    # SHAP Variable importance
    plt.figure()
    shap.summary_plot(shap_values, xtest, plot_type="bar", show=False)
    plt.savefig(imgSummaryBar, bbox_inches="tight")

    # SHAP Variable importance with values
    plt.figure()
    shap.summary_plot(shap_values, xtest, show=False)
    plt.savefig(imgSummary, bbox_inches="tight")
 
    # Waterfall plot of one sample
    plt.figure()
    shap.plots._waterfall.waterfall_legacy(explainer.expected_value, shap_values[0], max_display=20, feature_names=xtrain.columns, show=False)
    plt.savefig(imgWaterfall, bbox_inches="tight")
        
    # Force plot of one sample
    plt.figure()
    shap.plots.force(explainer.expected_value, shap_values[0], matplotlib=True, feature_names=xtrain.columns, show=False)
    plt.savefig(imgForce, bbox_inches="tight")

def shapValuesLinear(model, xtrain, xtest, imgSummaryBar, imgSummary, imgWaterfall, imgForce):
    plt.figure()
    explainer = shap.LinearExplainer(model, xtrain)
    shap_values = explainer.shap_values(xtest)
    
    # SHAP Variable importance
    shap.summary_plot(shap_values, xtest, plot_type="bar", show=False)
    plt.savefig(imgSummaryBar, bbox_inches="tight")
    plt.figure()
    
    # SHAP Variable importance with values
    shap.summary_plot(shap_values, xtest, show=False)
    plt.savefig(imgSummary, bbox_inches="tight")
    plt.figure()

    # Waterfall plot of one sample
    shap.plots._waterfall.waterfall_legacy(explainer.expected_value, shap_values[0], max_display=20, feature_names=xtrain.columns, show=False)
    plt.savefig(imgWaterfall, bbox_inches="tight")
    plt.figure()

    # Force plot of one sample
    shap.plots.force(explainer.expected_value, shap_values[0], matplotlib=True, feature_names=xtrain.columns, show=False)
    plt.savefig(imgForce, bbox_inches="tight")
    plt.figure()
    
def samplingExplainer(model, xtrain, xtest, imgSummaryBar, imgSummary, imgWaterfall, imgForce):
    plt.figure()
    explainer = shap.explainers.Sampling(model.predict, xtrain)
    shap_values = explainer.shap_values(xtest)
    
    # SHAP Variable importance
    shap.summary_plot(shap_values, xtest, plot_type="bar", show=False)
    plt.savefig(imgSummaryBar, bbox_inches="tight")
    plt.figure()
    
    # SHAP Variable importance with values
    shap.summary_plot(shap_values, xtest, show=False)
    plt.savefig(imgSummary, bbox_inches="tight")
    plt.figure()

    # Waterfall plot of one sample
    shap.plots._waterfall.waterfall_legacy(explainer.expected_value, shap_values[0], max_display=20, feature_names=xtrain.columns, show=False)
    plt.savefig(imgWaterfall, bbox_inches="tight")
    plt.figure()

    # Force plot of one sample
    shap.plots.force(explainer.expected_value, shap_values[0], matplotlib=True, feature_names=xtrain.columns, show=False)
    plt.savefig(imgForce, bbox_inches="tight")
    plt.figure()
#%%
print(x_train_std.mean())
print(X_train_r.mean())
#%%
print(x_test_std.iloc[0, :])
print(X_train_r.iloc[0, :])
#%% Finalne verzije
#Stablo odluke
regressor = DecisionTreeRegressor(criterion='mse', max_depth=15, max_features='auto', splitter='best')
regressor.fit(x_train_std, Y_train)
y_pred = regressor.predict(x_test_std)
[mse, mae, rmse, r2, r2_adj, rss] = model_evaluation(Y_test, y_pred, x_train_std.shape[0], x_train_std.shape[1])
model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)

perm_importance(regressor, x_test_std, Y_test, 'stablaPermImp.jpg', False)

shapValuesTree(regressor, x_train_std, x_test_std, 'stablaSummaryBar.jpg', 'stablaSummary.jpg', 'stablaWaterfall.jpg', 'stablaForce.jpg')

#%%

regressor = DecisionTreeRegressor(criterion='mse', max_depth=20, max_features='auto', splitter='best')
regressor.fit(X_train_r, Y_train)
y_pred = regressor.predict(X_test_r)
[mse, mae, rmse, r2, r2_adj, rss] = model_evaluation(Y_test, y_pred, X_train_r.shape[0], X_train_r.shape[1])
model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)

perm_importance(regressor, X_test_r, Y_test, 'stablaPCAPermImp.jpg', False)

shapValuesTree(regressor, X_train_r, X_test_r, 'stablaPCASummaryBar.jpg', 'stablaPCASummary.jpg', 'stablaPCAWaterfall.jpg', 'stablaPCAForce.jpg')

#%% Random forest

regressor = RandomForestRegressor(criterion='mse', max_features='auto', n_estimators=35)
regressor.fit(x_train_std, Y_train)
y_pred = regressor.predict(x_test_std)
[mse, mae, rmse, r2, r2_adj, rss] = model_evaluation(Y_test, y_pred, x_train_std.shape[0], x_train_std.shape[1])
model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)

perm_importance(regressor, x_test_std, Y_test, 'rfPermImp.jpg', False)

shapValuesTree(regressor, x_train_std, x_test_std, 'rfSummaryBar.jpg', 'rfSummary.jpg', 'rfWaterfall.jpg', 'rfForce.jpg')

#%%

regressor = RandomForestRegressor(criterion='mae', max_features='auto', n_estimators=100)
regressor.fit(X_train_r, Y_train)
y_pred = regressor.predict(X_test_r)
[mse, mae, rmse, r2, r2_adj, rss] = model_evaluation(Y_test, y_pred, X_train_r.shape[0], X_train_r.shape[1])
model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)

perm_importance(regressor, X_test_r, Y_test, 'rfPCAPermImp.jpg', False)

shapValuesTree(regressor, X_train_r, X_test_r, 'rfPCASummaryBar.jpg', 'rfPCASummary.jpg', 'rfPCAWaterfall.jpg', 'rfPCAForce.jpg')

#%%
#KNN
regressor = KNeighborsRegressor(metric='braycurtis', n_neighbors=10, weights='distance')
regressor.fit(x_train_std, Y_train)
y_pred = regressor.predict(x_test_std)
[mse, mae, rmse, r2, r2_adj, rss] = model_evaluation(Y_test, y_pred, x_train_std.shape[0], x_train_std.shape[1])
model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)

perm_importance(regressor, x_test_std, Y_test, 'KNNPermImp.jpg', False)

knnShap = samplingExplainer(regressor, x_train_std, x_test_std, 'KNNummaryBar.jpg', 'KNNSummary.jpg', 'KNNWaterfall.jpg', 'KNNForce.jpg')

#%%

regressor = KNeighborsRegressor(metric='chebyshev', n_neighbors=9, weights='distance')
regressor.fit(X_train_r, Y_train)
y_pred = regressor.predict(X_test_r)
[mse, mae, rmse, r2, r2_adj, rss] = model_evaluation(Y_test, y_pred, X_train_r.shape[0], X_train_r.shape[1])
model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)

perm_importance(regressor, X_test_r, Y_test, 'KNNPCAPermImp.jpg', False)
knnShap = samplingExplainer(regressor, X_train_r, X_test_r, 'KNNPCASummaryBar.jpg', 'KNNPCASummary.jpg', 'KNNPCAWaterfall.jpg', 'KNNPCAForce.jpg')

#%%    
#Linearna regresija
lasso_model = Lasso(alpha=0.1)
lasso_model.fit(x_train_intOnly2, Y_train)
Y_pred = lasso_model.predict(x_test_intOnly2)

[mse, mae, rmse, r2, r2_adj, rss] = model_evaluation(Y_test, Y_pred, x_train_intOnly2.shape[0], x_train_intOnly2.shape[1])
model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)

perm_importance(lasso_model, x_test_intOnly2, Y_test, 'LinRegPermImp.jpg', True, True)

shapValuesLinear(lasso_model, x_train_intOnly2, x_test_intOnly2, 'linRegSummaryBar.jpg', 'linRegSummary.jpg', 'linRegWaterfall.jpg', 'linRegForce.jpg')

#%%
lasso_model = Lasso(alpha=0.1)
lasso_model.fit(x_train_int2PCA, Y_train)
Y_pred = lasso_model.predict(x_test_int2PCA)

[mse, mae, rmse, r2, r2_adj, rss] = model_evaluation(Y_test, Y_pred, x_train_int2PCA.shape[0], x_train_int2PCA.shape[1])
model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)

perm_importance(lasso_model, x_test_int2PCA, Y_test, 'LinRegPCAPermImp.jpg', True, True)

shapValuesLinear(lasso_model, x_train_int2PCA, x_test_int2PCA, 'linRegPCASummaryBar.jpg', 'linRegPCASummary.jpg', 'linRegPCAWaterfall.jpg', 'linRegPCAForce.jpg')

#%% Obicna linearna regresija, za potrebe poredjenja
linear_model = LinearRegression(fit_intercept=True)
linear_model.fit(x_train_std, Y_train)
Y_pred = linear_model.predict(x_test_std)

[mse, mae, rmse, r2, r2_adj, rss] = model_evaluation(Y_test, Y_pred, x_train_std.shape[0], x_train_std.shape[1])
model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)

perm_importance(linear_model, x_test_std, Y_test, 'LinRegNormalPermImp.jpg', True)

shapValuesLinear(linear_model, x_train_std, x_test_std, 'linRegNormalSummaryBar.jpg', 'linRegNormalSummary.jpg', 'linRegNormalWaterfall.jpg', 'linRegNormalForce.jpg')

#%%
linear_model = LinearRegression(fit_intercept=True)
linear_model.fit(X_train_r, Y_train)
Y_pred = linear_model.predict(X_test_r)

[mse, mae, rmse, r2, r2_adj, rss] = model_evaluation(Y_test, Y_pred, X_train_r.shape[0], X_train_r.shape[1])
model_evaluation_print(mse, mae, rmse, r2, r2_adj, rss)

perm_importance(linear_model, X_test_r, Y_test, 'LinRegPCANormalPermImp.jpg', True, True)

shapValuesLinear(linear_model, X_train_r, X_test_r, 'linRegPCANormalSummaryBar.jpg', 'linRegPCANormalSummary.jpg', 'linRegPCANormalWaterfall.jpg', 'linRegPCANormalForce.jpg')
#%%
